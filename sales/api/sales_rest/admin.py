from django.contrib import admin
from sales_rest.models import Salesperson, Customer, AutomobileVO, Sale


@admin.register(Salesperson)
class Salesperson(admin.ModelAdmin):
    pass


@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass


@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass


@admin.register(Sale)
class Sale(admin.ModelAdmin):
    pass
