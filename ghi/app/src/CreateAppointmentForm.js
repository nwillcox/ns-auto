import React, {useEffect, useState} from 'react';


function CreateAppointmentForm() {
    const [technicians, setTechnicians] = useState([]);
    const [alertState, setAlertState] = useState('alert alert-success d-none');
    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date_time: '',
        technician: '',
        reason: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        };
    };

    function sleep(ms) {
        return (new Promise(resolve => setTimeout(resolve, ms)));
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: '',
                customer: '',
                date_time: '',
                technician: '',
                reason: '',
            });
            setAlertState("mt-3 alert alert-success");
            await sleep(3000);
            setAlertState("mt-3 alert alert-success d-none");
        };
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input value={formData.vin} onChange={handleFormChange} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control"/>
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.customer} onChange={handleFormChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.date_time} onChange={handleFormChange} placeholder="DateTime" type="datetime-local" name="date_time" id="date_time" className="form-control"/>
                            <label htmlFor="date_time">Date Time</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.technician} onChange={handleFormChange} required name="technician" id="technician" className="form-select">
                            <option value="">Choose a technician..</option>
                            {technicians?.map(technician => {
                                return(
                                    <option key={ technician.id } value={ technician.id }>{ technician.first_name } { technician.last_name }</option>
                                );
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.reason} onChange={handleFormChange} placeholder="Reason" required type="textfield" name="reason" id="reason" className="form-control"/>
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                    <div className={alertState} role="alert">
                        Appointment Submitted
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateAppointmentForm
