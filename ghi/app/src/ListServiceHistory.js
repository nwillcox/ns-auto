import React, { useEffect, useState } from 'react';


function ListServiceHistory() {
    const [serviceHistoryList, setServiceHistoryList] = useState([]);
    const [filteringList, setFilteringList] = useState([]);
    const [filter, setFilter] = useState("");

    const handleFilterChange = (event) => {
        const value = event.target.value;
        setFilter(value);
    };

    async function loadServiceHistory() {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setServiceHistoryList(data.appointments);
            setFilteringList(data.appointments);
        };
    };

    useEffect(() => {
        loadServiceHistory();
    }, []);

    function prettyDateTime(isodatetime) {
        return (new Date(isodatetime).toLocaleString('en-US', {
            timeZone: 'UTC',
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
            hour: '2-digit',
            minute: '2-digit',
        }));
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        const filtered = serviceHistoryList.filter(appointment => appointment.vin === filter);
        setFilteringList(filtered);
    };

    return (
        <>
            <h1>Service History</h1>
            <form onSubmit={handleSubmit}>
                <div className="input-group">
                <input onChange={handleFilterChange} type="text" className="form-control" placeholder="Search by VIN..."/>
                <div className="input-group-append">
                    <button className="btn btn-outline-secondary" disabled={filter === ''}>Search</button>
                </div>
                </div>
            </form>
            <button onClick={() => {setFilteringList(serviceHistoryList)}}>Reset Filter</button>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date & Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {filteringList?.map(appointment => {
                        return (
                            <tr key={ appointment.id }>
                                <td>{ appointment.vin }</td>
                                <td>{ appointment.vip === true? "Yes": "No" }</td>
                                <td>{ appointment.customer }</td>
                                <td>{ prettyDateTime(appointment.date_time) }</td>
                                <td>{ appointment.technician.first_name } { appointment.technician.last_name }</td>
                                <td>{ appointment.reason }</td>
                                <td>{ appointment.status }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ListServiceHistory;
