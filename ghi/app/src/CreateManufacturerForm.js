import React, { useState } from 'react';


function CreateManufacturerForm() {
    const [name, setName] = useState("");
    const [alertState, setAlertState] = useState('alert alert-success d-none');

    function sleep(ms) {
        return (new Promise(resolve => setTimeout(resolve, ms)));
    };

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = name;
        const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(manufacturersUrl, fetchConfig);
        if (response.ok) {
            setName('');
            setAlertState("mt-3 alert alert-success");
            await sleep(3000);
            setAlertState("mt-3 alert alert-success d-none");
        };
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input value={name} onChange={handleNameChange} placeholder="Manufacturer name..." required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Manufacturer Name</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                    <div className={alertState} role="alert">
                        Manufacturer Added
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateManufacturerForm;
