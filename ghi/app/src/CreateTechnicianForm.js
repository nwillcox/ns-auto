import React, {useState} from 'react';


function CreateTechnicianForm() {
    const [formData, setFormData] = useState({
        employee_id: '',
        first_name: '',
        last_name: '',
    });
    const [alertState, setAlertState] = useState('alert alert-success d-none');

    function sleep(ms) {
        return (new Promise(resolve => setTimeout(resolve, ms)));
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                employee_id: '',
                first_name: '',
                last_name: '',
            });
            setAlertState("mt-3 alert alert-success");
            await sleep(3000);
            setAlertState("mt-3 alert alert-success d-none");
        };
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input value={formData.employee_id} onChange={handleFormChange} placeholder="Employee ID" required type="number" name="employee_id" id="employee_id" className="form-control"/>
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.first_name} onChange={handleFormChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control"/>
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.last_name} onChange={handleFormChange} placeholder="Last Name" type="text" name="last_name" id="last_name" className="form-control"/>
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                    <div className={alertState} role="alert">
                        Technician Added
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateTechnicianForm;
