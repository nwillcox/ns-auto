import React, { useState, useEffect } from 'react';


function ListSalespersonHistory() {
    const [salespersons, setSalespersons] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [sales, setSales] = useState([]);
    const [filteredSales, setFilteredSales] = useState([]);

    const fetchSalespersons = async () => {
        const url = 'http://localhost:8090/api/salespersons/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespersons);
        };
    };

    const fetchSales = async () => {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        };
    };

    useEffect(() => {
        fetchSalespersons();
        fetchSales();
    }, []);

    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        setSalesperson(value);
        const filteredSales = sales.filter(sale => sale.salesperson.employee_id === (value));
        setFilteredSales(filteredSales);
    };

    return (
        <>
            <h1>Salesperson History</h1>
            <div className="mb-3">
                <select value={salesperson} onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select" multiple={false}>
                    <option value="">Choose a Salesperson</option>
                    {salespersons?.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.employee_id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        );
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson</th>
                        <th>Salesperson Employee ID</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filteredSales?.map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>{ sale.salesperson.first_name } { sale.salesperson.last_name }</td>
                                <td>{ sale.salesperson.employee_id }</td>
                                <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>${ sale.price }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ListSalespersonHistory;
