import React, {useEffect, useState} from 'react';


function CreateSaleForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [automobile, setAutomobile] = useState('');
    const [salespersons, setSalespersons] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [customers, setCustomers] = useState([]);
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState(0);
    const [alertState, setAlertState] = useState('alert alert-success d-none');

    const fetchAutomobileData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        };
    };

    const fetchSalespersonData = async () => {
        const url = 'http://localhost:8090/api/salespersons/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespersons(data.salespersons);
        };
    };

    const fetchCustomerData = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        };
    };

    function sleep(ms) {
        return (new Promise(resolve => setTimeout(resolve, ms)));
    };

    useEffect(() => {
        fetchAutomobileData();
        fetchSalespersonData();
        fetchCustomerData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/sales/`;
        const newSale = {
            automobile,
            salesperson,
            customer,
            price,
        };
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newSale),
            headers: {
                'Content-Type': 'application.json',
            },
        };
        const response = await fetch(url, fetchConfig);
        const automobileURL = `http://localhost:8100${automobile}`;
        const soldConfig = {
            method: "PUT",
            body: JSON.stringify({ sold: true }),
            headers: {
                'Content-Type': 'application.json',
            },
        };

        await fetch(automobileURL, soldConfig);

        if (response.ok) {
            fetchAutomobileData();
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
            setAlertState("mt-3 alert alert-success");
            await sleep(3000);
            setAlertState("mt-3 alert alert-success d-none");
        };
    };

    const handleAutomobileChange = (e) => {
        const value = e.target.value;
        setAutomobile(value);
    };

    const handleSalespersonChange = (e) => {
        const value = e.target.value;
        setSalesperson(value);
    };

    const handleCustomerChange = (e) => {
        const value = e.target.value;
        setCustomer(value);
    };

    const handlePriceChange = (e) => {
        const value = e.target.value;
        setPrice(value);
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a Sale</h1>
                    <form onSubmit={handleSubmit} id="create_sale_form">
                        <div className="mb-3">
                            <select value={automobile} onChange={handleAutomobileChange} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an Automobile</option>
                                {automobiles.filter(automobiles => automobiles.sold === false)?.map(automobile => {
                                    return (
                                        <option key={ automobile.href } value={ automobile.href }>
                                            { automobile.year } { automobile.model.manufacturer.name } { automobile.model.name }
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={salesperson} onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose a Salesperson</option>
                                {salespersons?.map(salesperson => {
                                    return (
                                        <option key={ salesperson.id } value={ salesperson.employee_id }>
                                            { salesperson.first_name } { salesperson.last_name }
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select value={customer} onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {customers?.map(customer => {
                                    return (
                                        <option key={ customer.id } value={ customer.id }>
                                            { customer.first_name } { customer.last_name }
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={price} onChange={handlePriceChange} placeholder="Price" required type="number" name="price" id="price" className="form-control"/>
                            <label htmlFor="price">Price</label>
                        </div>
                        <button className="btn btn-primary">Record</button>
                    </form>
                    <div className={alertState} role="alert">
                        Sale Recorded
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateSaleForm;
