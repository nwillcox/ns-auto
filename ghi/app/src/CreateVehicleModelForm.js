import React, {useEffect, useState} from 'react';


function CreateVehicleModelForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [alertState, setAlertState] = useState('alert alert-success d-none');
    const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer_id: '',
    });

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        };
    };

    function sleep(ms) {
        return (new Promise(resolve => setTimeout(resolve, ms)));
    };

        useEffect(() => {
            fetchData();
        }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(modelUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                name: '',
                picture_url: '',
                manufacturer_id: '',
            });
            setAlertState("mt-3 alert alert-success");
            await sleep(3000);
            setAlertState("mt-3 alert alert-success d-none");
        };
    };

    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Vehicle Model</h1>
                    <form onSubmit={handleSubmit} id="create-model-form">
                        <div className="form-floating mb-3">
                            <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                            <label htmlFor="url">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select value={formData.manufacturer_id} onChange={handleFormChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                            <option value="">Choose a manufacturer</option>
                            {manufacturers?.map(manufacturer => {
                                return(
                                    <option key={ manufacturer.id } value={ manufacturer.id }>{ manufacturer.name }</option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                    <div className={alertState} role="alert">
                        Model Added
                    </div>
                </div>
            </div>
        </div>
    );
};

export default CreateVehicleModelForm;
