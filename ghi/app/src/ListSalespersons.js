import React, { useEffect, useState } from 'react';


function ListSalespersons() {
    const [salespersonsList, setSalespersonsList] = useState([]);
    async function loadSalespersons() {
        const response = await fetch('http://localhost:8090/api/salespersons/');
        if (response.ok) {
            const data = await response.json();
            setSalespersonsList(data.salespersons);
        };
    };

    useEffect(() => {
        loadSalespersons();
    }, []);

    return (
        <>
            <h1>Salespersons</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersonsList.map(salesperson => {
                        return (
                            <tr key={ salesperson.id }>
                                <td>{ salesperson.employee_id }</td>
                                <td>{ salesperson.first_name }</td>
                                <td>{ salesperson.last_name }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ListSalespersons;
